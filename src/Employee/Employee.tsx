import { Competence } from './Competence/Competence';

export class Employee {
    public firstName: string;
    public surname: string;
    public yearsExperience: number;
    public competencies: Competence[];

	constructor(firstName:string, surname: string, yearsExperience: number ){
        this.firstName = firstName;
        this.surname = surname;
        this.yearsExperience = yearsExperience;
        this.competencies = new Array<Competence>();
	}    
}
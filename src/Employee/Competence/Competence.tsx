export class Competence {
    public name: string;
    public yearsExperience: number;
    public isSaved: boolean;
    public id: number;

	constructor(name:string, yearsExperience: number, isSaved: boolean ){
        this.name = name;
        this.yearsExperience = yearsExperience;
        this.isSaved = isSaved;
        this.id = Math.floor(Math.random() * Math.floor(10000000000)); // Temporary kludge to provide a unique(enough for demo) id.
	}    
}
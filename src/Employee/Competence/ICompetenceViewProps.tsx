import { Competence } from './Competence';

export interface ICompetenceViewProps {
    competence: Competence;
}
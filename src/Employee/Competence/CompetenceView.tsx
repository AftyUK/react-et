import update from 'immutability-helper';
import * as React from 'react';
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Grid, HelpBlock, Jumbotron, Row } from 'react-bootstrap';
import './CompetencyView.css';
import { ICompetenceViewProps } from './ICompetenceViewProps';

class CompetenceView extends React.Component<ICompetenceViewProps, {}> {
    public state: any;
    public props: any;

    constructor(props: ICompetenceViewProps) {
        super(props);
        this.state = {
            competence: this.props.competence,
            validating: this.props.competence.isSaved

        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleYearsExperienceChange = this.handleYearsExperienceChange.bind(this);
    }

    public render() {
        return (
            <Form>
                <Grid>
                    <Row>
                        <Col sm={2}>
                            <FormGroup controlId="yearsExperience" validationState={this.getYearsExperienceValidationState()} className="years-experience" >
                                <FormControl type="number" value={this.state.competence.yearsExperience} placeholder="Years" onChange={this.handleYearsExperienceChange} />
                                <FormControl.Feedback />                                
                            </FormGroup>
                        </Col>
                        <Col sm={2} className="years-of-label">
                            Years Of
                        </Col>                        
                        <Col sm={3}>
                            <FormGroup controlId="name" validationState={this.getNameValidationState()} >
                                <FormControl type="text" value={this.state.competence.name} placeholder="Name of competency" onChange={this.handleNameChange} className="competence-name" />
                                <FormControl.Feedback />                                
                            </FormGroup>
                        </Col>
                        <Col sm={3}>
                            <Button>Save</Button>
                        </Col>
                    </Row>
                </Grid>
            </Form>
        );
    }


    protected getNameValidationState() {
        if (this.state.validating) {
            const length = this.state.competence.name.length;
            if (length > 1 && length < 43) { return 'success' }
            else { return 'error' };
        } else {
            return null;
        }
    }

    protected getYearsExperienceValidationState() {
        if (this.state.validating) {
            const val = +(this.state.competence.yearsExperience);
            if (val > 0) { return 'success' }
            else { return 'error' };
        } else {
            return null;
        }
    }

    protected handleNameChange(event: any) {
        this.state.validating = true;
        const newDetails = update(this.state.competence, { name: { $set: event.target.value } });
        this.setState({ competence: newDetails });
    }

    protected handleYearsExperienceChange(event: any) {
        this.state.validating = true;        
        const newDetails = update(this.state.competence, { yearsExperience: { $set: event.target.value } });
        this.setState({ competence: newDetails });
    }
}

export default CompetenceView;

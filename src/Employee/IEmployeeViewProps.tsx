import { Employee } from './Employee';

export interface IEmployeeViewProps {
    employee: Employee;
}
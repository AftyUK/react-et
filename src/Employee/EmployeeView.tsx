import update from 'immutability-helper';
import * as React from 'react';
import { Button, Col, ControlLabel, Form, FormControl, FormGroup, Grid, HelpBlock, Jumbotron, Row } from 'react-bootstrap';
import { Competence } from './Competence/Competence';
import CompetenceView from './Competence/CompetenceView';
import './EmployeeView.css';
import { IEmployeeViewProps } from './IEmployeeViewProps';

class EmployeeView extends React.Component<IEmployeeViewProps, {}> {
  public state: any; 
  public props: any; // Having these as public seems very strange, but perhaps a strange artifact of React? It seems they must be public

  constructor(props: IEmployeeViewProps) {
    super(props);
    this.state = {
      employee: this.props.employee
    };

    this.handleFirstNameChange = this.handleFirstNameChange.bind(this); // Bit repetitive, must be a nicer way to do this.
    this.handleSurnameChange = this.handleSurnameChange.bind(this);
    this.handleYearsExperienceChange = this.handleYearsExperienceChange.bind(this);
    this.handleAddCompetencyClick = this.handleAddCompetencyClick.bind(this);
    this.handleDeleteCompetencyClick = this.handleDeleteCompetencyClick.bind(this);
    
  }

  public render() {
    return (
      <Grid>
        <Form horizontal={true}>
          <Row>
            <Col className="text-center">
              <h2>{this.state.employee.firstName} {this.state.employee.surname} - {this.state.employee.yearsExperience} total years of experience</h2>
              <hr />
            </Col>
          </Row>
          <Row>
            <Col>
              <Jumbotron>
                <h4> Employee Details </h4>
                <hr />
                <FormGroup controlId="employeeFirstName" validationState={this.getFirstNameValidationState()} >
                  <Col componentClass={ControlLabel} sm={4}>
                    First Name
                </Col>
                  <Col sm={8}>
                    <FormControl type="text" value={this.state.employee.firstName} placeholder="First Name" onChange={this.handleFirstNameChange} />
                    <FormControl.Feedback />
                    <HelpBlock>Please enter a first name of 3 characters or more.</HelpBlock>
                  </Col>
                </FormGroup>

                <FormGroup controlId="employeeSurname" validationState={this.getSurnameValidationState()} >
                  <Col componentClass={ControlLabel} sm={4}>
                    Surname
                </Col>
                  <Col sm={8}>
                    <FormControl type="text" value={this.state.employee.surname} placeholder="Surname" onChange={this.handleSurnameChange} />
                    <FormControl.Feedback />
                    <HelpBlock>Please enter a surname of 3 characters or more.</HelpBlock>
                  </Col>
                </FormGroup>

                <FormGroup controlId="employeeYearsExperience" validationState={this.getYearsExperienceValidationState()} >
                  <Col componentClass={ControlLabel} sm={4}>
                    Experience (Years)
                </Col>
                  <Col sm={8}>
                    <FormControl type="number" value={this.state.employee.yearsExperience} placeholder="e.g. 4" onChange={this.handleYearsExperienceChange} />
                    <FormControl.Feedback />
                    <HelpBlock>Please record how many years experience this employee has - zero or more.</HelpBlock>
                  </Col>
                </FormGroup>
              </Jumbotron>
            </Col>
          </Row>
          </Form>

          <Row>
            <Col>
              <Jumbotron>
                <h4> Competencies</h4>
                <hr />
                <ul>
                  { /* tslint:disable */  
                  // Disabling TSLint so we can use an arrow/lambda function. This is bad for performance, and should be refactored into the component given more time
                  // Avoiding arrow functions in render is good for performance, but I didn't find a workaround in time
                  }
                  {this.state.employee.competencies.map((comp: Competence) => ( 
                    <li key={comp.id}> 
                      <Button className="delete-button" onClick={() => this.handleDeleteCompetencyClick(comp.id)}>X</Button>                    
                      <CompetenceView competence={comp} />
                    </li>
                  ))}
                  <li>
                    <Row>
                      <Col smOffset={7}>
                        <Button onClick={this.handleAddCompetencyClick}>Add Competency</Button>
                      </Col>
                    </Row>
                  </li>
                </ul>
              </Jumbotron>
            </Col>
          </Row>

      </Grid>
    );
  }

  protected getFirstNameValidationState() {
    // These validation methods  feel like they're a bit repetitive, must be some opportunities for code re-use. Handling each field separately feels wasteful.
    const length = this.state.employee.firstName.length;
    if (length > 2) { return 'success' }
    else { return 'error' };
  }

  protected getSurnameValidationState() {
    const length = this.state.employee.surname.length;
    if (length > 2) { return 'success' }
    else { return 'error' };
  }

  protected getYearsExperienceValidationState() {
    const val = +(this.state.employee.yearsExperience);
    if (val >= 0) { return 'success' }
    else { return 'error' };
  }

  protected handleFirstNameChange(event: any) {
    const newDetails = update(this.state.employee, { firstName: { $set: event.target.value } }); 
    // We are using immutability-helper to keep' state.employee' immutable for performance. Again I don't like the repetitiveness on these methods.
    this.setState({ employee: newDetails });
  }

  protected handleSurnameChange(event: any) {
    const newDetails = update(this.state.employee, { surname: { $set: event.target.value } });
    this.setState({ employee: newDetails });
  }

  protected handleYearsExperienceChange(event: any) {
    const newDetails = update(this.state.employee, { yearsExperience: { $set: event.target.value } });
    this.setState({ employee: newDetails });
  }

  protected handleAddCompetencyClick(event: any) {
    const newComps = update(this.state.employee.competencies, { $push: [ new Competence("", 1, false)] });
    const newDetails = update(this.state.employee, { competencies: { $set: newComps } } );
    this.setState({ employee: newDetails });
  }

  protected handleDeleteCompetencyClick(id: number) {
    let comps = this.state.employee.competencies;
    // Find index of matching competence
    var foundIndex = -1;
    for (var i=0; i<comps.length; i++) {
      if(+(comps[i].id) === +id){
        foundIndex = i;
      }
    }    
    if(foundIndex > -1){ // Feels like I'm missing a trick on immutability here, would like to review
      comps.splice(foundIndex, 1);
      const newDetails = update(this.state.employee, { competencies: { $set: comps } } );
      this.setState({ employee: newDetails });      
    }
}

  
}

export default EmployeeView;

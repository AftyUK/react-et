import 'bootstrap/dist/css/bootstrap-theme.css';
import 'bootstrap/dist/css/bootstrap.css';
import * as React from 'react';
import { Col, Grid, Navbar, Row } from 'react-bootstrap';
import * as ReactDOM from 'react-dom';
import { Competence } from './Employee/Competence/Competence';
import { Employee } from './Employee/Employee';
import EmployeeView from './Employee/EmployeeView';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const emp = new Employee('Russ', 'Davies', 19);
emp.competencies.push(new Competence('C#', 16, true));
emp.competencies.push(new Competence('HTML', 19, true));
emp.competencies.push(new Competence('CSS', 19, true));
emp.competencies.push(new Competence('SQL', 17, true));
emp.competencies.push(new Competence('Java', 2, true));

ReactDOM.render(
  <div>
  <Navbar inverse={true} fixedTop={true}>
      <Grid>
          <Navbar.Header>
              <Navbar.Brand>
                  <h1>Experience Tracker</h1>
              </Navbar.Brand>
              <Navbar.Toggle />
          </Navbar.Header>
      </Grid>
  </Navbar>
  <Grid className="content-container">
    <Row>
      <Col>
        <EmployeeView employee={emp} />
      </Col>
    </Row>
  </Grid>
</div>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
